import { lazy } from 'react';
// Dynamic imports
const LazyContacts = lazy(() => {
    // Prevent unpleasant blinking behaviour due to fast loadtimes by introducing a minimum delay
    return Promise.all([
        import('../../modules/contacts/index'),
        new Promise(resolve => setTimeout(resolve, 1500))
    ]).then(([moduleExports]) => moduleExports);
    });
const LazySpam = lazy(() => {
    return Promise.all([
        import('../../modules/spam/index'),
        new Promise(resolve => setTimeout(resolve, 1500))
    ]).then(([moduleExports]) => moduleExports);
    });
const LazyTrash = lazy(() => {
    return Promise.all([
        import('../../modules/trash/index'),
        new Promise(resolve => setTimeout(resolve, 1500))
    ]).then(([moduleExports]) => moduleExports);
    });



const LazyConfig = [
        {
            name: 'Contacts',
            path: '/contacts',
            component: LazyContacts
        },
        {
            name: 'Spam',
            path: '/spam',
            component: LazySpam
        },
        {
            name: 'Trash',
            path: '/trash',
            component: LazyTrash
        },
    ]

export default LazyConfig