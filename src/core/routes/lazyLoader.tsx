import React, { Suspense } from "react";
import LazyProgress from "./components/lazyProgress";


const LazyLoad /* : React.FC */ = (obj: {
  name: string;
  path: string;
  component: any;
}) => {
  return (
      <Suspense fallback={<LazyProgress />}>
        <obj.component />
      </Suspense>
  );
};

export default LazyLoad;
