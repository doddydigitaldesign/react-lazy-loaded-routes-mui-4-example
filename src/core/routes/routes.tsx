import React, { PureComponent } from "react";
import { Switch, Route } from "react-router-dom";
import LazyLoad from "./lazyLoader";
import LazyConfig from "./lazyConfig";


interface Props {}

export default class RoutingTable extends PureComponent<Props> {
  render() {
    const components = LazyConfig;
    return (
      <Switch>
        {components.map((comp, i) => {
          return (
            <Route key={i} path={comp.path} render={() => LazyLoad(comp)} />
          );
        })}
      </Switch>
    );
  }
}
