import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import LazyConfig from '../../../routes/lazyConfig';
import Tab from '@material-ui/core/Tab/Tab';
import { Link } from 'react-router-dom';




export default function TabsComponent(props: { children: React.ReactNode; }) {
  const [value, setValue] = React.useState(0);
  const [state, setState] = React.useState(false)

  function handleChange(event: React.ChangeEvent<{}>, newValue: number) {
    setValue(newValue);
  }

  return (
        <Tabs
        value={value}
        indicatorColor="primary"
        textColor="primary"
        onChange={handleChange}
        aria-label="disabled tabs example"
      >
        {LazyConfig.map((tab, i) => (<Tab key={i} label={tab.name} component={Link} to={tab.path} />))}
         {props.children}
      </Tabs>
  );
}