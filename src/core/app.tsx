import React from 'react'
import ClippedDrawer from './layout'
import RoutingTable from './routes/routes'


interface Props {
    
}



const App: React.FC<Props> = () => {
    return (
        <div>
        <ClippedDrawer>
            <RoutingTable />
        </ClippedDrawer>
        </div>
    )
}

export default App
