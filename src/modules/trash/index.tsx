import React from "react";
import Typography from "@material-ui/core/Typography/Typography";

interface Props {}

const Trash: React.FC<Props> = () => {
  return (
    <div>
      <Typography component="h1" variant="h1">
        Trash
      </Typography>
    </div>
  );
};

export default Trash;
