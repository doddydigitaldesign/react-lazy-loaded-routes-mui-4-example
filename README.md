

# Lazy-loaded Routing Demo (CRA TypeScript App) 
[![language: typescript](https://img.shields.io/badge/language-typescript%20-blue)](https://github.com/Microsoft/TypeScript)
[![framework: react](https://img.shields.io/badge/framework-react%20-blue)](https://github.com/facebook/react)
[![code style: prettier tslint](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
----------

This is a simple demo of how one can setup lazy-loading of modules with react-router-dom.

## Getting started
1. Clone the repository
2. Install the dependencies: `npm install`
3. Run the app locally: `npm start`

